# AtomIDE Plugins

Репозиторий для хранения списка плагинов, и демонстрации их работы в видео ролике.

## Cписок плагинов

Список плагинов, представленный в виде таблички с кратким описанием.
Столбец описание не является инструкцией по настройке, более подробно описано в репозитории самого плагина.


| Плагин      | Описание    | Github Link   |
| :---        |    :----:   |          ---: |
| highlight-selected | плагин с помощью которого, при выделении строчки, все совпадающие строчки будут подсвечены | [Atom Lib](https://atom.io/packages/highlight-selected) [Github](https://github.com/richrace/highlight-selected)  |
| file-icons | плагин для отображения корректных иконок файлов в структуре, исходя из типа файла | [Atom Lib](https://atom.io/packages/file-icons) [GitHub](https://github.com/file-icons/atom) |
| open-recent   | добавляет возможность открыть последние открытые и используемые файлы | [Atom Lib](https://atom.io/packages/open-recent) [GitHub](https://github.com/Zren/atom-open-recent)  |
| atom-beautify | Приводит файл к более читабельному виду | [Atom Lib](https://atom.io/packages/atom-beautify) [GitHub](https://github.com/Glavin001/atom-beautify) |
| platformio-ide-terminal  | Плагин с терминалом для IDE | [Atom Lib](https://atom.io/packages/platformio-ide-terminal) [GitHub](https://github.com/platformio/platformio-atom-ide-terminal)   |
| autocomplete-paths | Автокомплишн для путей | [Atom Lib](https://atom.io/packages/autocomplete-paths) [GitHub](https://github.com/atom-community/autocomplete-paths) |
| tab-switcher   | Переключение между открытыми вкладками - alt+ '[' или ']'| [Atom Lib](https://atom.io/packages/tab-switcher) [GitHub](https://github.com/oggy/tab-switcher) |
| teletype | Демо видео ответит само за себя, кратко - шаринг IDE | [Atom Lib](https://atom.io/packages/teletype) [Demo](https://teletype.atom.io/) |
| jumpy | Плагин с помощью которого можно передвигаться по проекту с помощью hotkeys | [Atom Lib](https://atom.io/packages/jumpy) [GitHub](https://github.com/DavidLGoldberg/jumpy) |
| pretty-json | Делает json читаемым        | [Atom Lib](https://atom.io/packages/pretty-json) [GitHub](https://github.com/federomero/pretty-json) |
| auto-update-packages   | Для тех, кому лень вручную обновлять плагины, этот плагин сделает все за вас| [Atom Lib](https://atom.io/packages/auto-update-packages) [GitHub](https://github.com/yujinakayama/atom-auto-update-packages) |
| wakatime   | Интеграция IDE с https://wakatime.com        | [Atom Lib](https://atom.io/packages/wakatime) [GitHub](https://github.com/wakatime/atom-wakatime) |
| gitlab-manager | Показывает статус бар пайплайнов, можно быстро создать МР | [Atom Lib](https://atom.io/packages/gitlab-manager) [GitHub](https://github.com/T-Huelsken/gitlab-manager) |
| script | Детектит тип файла и позволяет его стартовать - `cmd+i` | [Atom Lib](https://atom.io/packages/script) [GitHub](https://github.com/atom-community/atom-script) |
| *************   | Линтеры и хайлайт | ************* |
| linter   | Дефолтный линтер для IDE | [Atom Lib](https://atom.io/packages/linter) [GitHub](https://github.com/steelbrain/linter) |
| linter-pylint   | Линтер для python файлов требует доустановки pylint и pylama | [Atom Lib](https://atom.io/packages/linter-pylint) [GitHub](https://github.com/AtomLinter/linter-pylint) |
| linter-docker   | dockerlint | [Atom Lib](https://atom.io/packages/linter-docker) [GitHub](https://github.com/AtomLinter/linter-docker) |
| linter-kubectl   | Линтер для kubernetes манифестов | [Atom Lib](https://atom.io/packages/linter-kubectl) [GitHub](https://github.com/mschuchard/linter-kubectl) |
| linter-ansible-linting | Линтер для плейбуков и ролей | [Atom Lib](https://atom.io/packages/linter-ansible-linting) [GitHub](https://github.com/mschuchard/linter-ansible-linting) |
| linter-js-yaml  | Линтер для yaml,yml расширений | [Atom Lib](https://atom.io/packages/linter-js-yaml) [GitHub](https://github.com/AtomLinter/linter-js-yaml) |
| language-docker | Подсветка docker синтаксиса | [Atom Lib](https://atom.io/packages/language-docker) [GitHub](https://github.com/jagregory/language-docker) |
| language-ansible | Подсветка ansible синтакса | [Atom Lib](https://atom.io/packages/language-ansible) [GitHub](https://github.com/haad/language-Ansible) |
| atom-password-generator | Удобный плагин для генерации случайного пароля `Ctrl+Alt+P` | [Atom Lib](https://atom.io/packages/atom-password-generator) [GitHub](https://github.com/maxkoryukov/atom-password-generator) |
| take-a-break | Заставляет сделать паузу в работе:) | [Atom Lib](https://atom.io/packages/take-a-break) [GitHub](https://github.com/ReiiYuki/Take-a-break) |
| Paragraph   | интеграция ansible-vault cli в atom | [Atom Lib](https://atom.io/packages/ansible-vault) [GitHub](https://github.com/sydro/atom-ansible-vault) |
| language-gitignore | .gitignore синтакс | [Atom Lib](https://atom.io/packages/language-gitignore) [GitHub](https://github.com/bsara/language-gitignore) |

Список будет дополняться, наверное....

***

## Установка плагинов с помощью .sh скрипта
Перед установкой, проверьте доступна ли тула `apm` из консоли.
Если нет - открыть Atom, открыть меню настроек Atom, выбрать `Install shell commands`

```
cd atomide
chmod +x plugins.sh
./plugins.sh
```

***
