#!/bin/bash

# Func check if maven installed
apm_check() {
    apm_equal=$(which apm)
    if [ -z "$apm_equal" ]
    then
        echo -e "\e[31mAPM atom manager is not installed\e[0m"
        exit 1;
    else
        echo -e "\e[32mAPM atom manager Ok installed\e[0m"
        apm -v
    fi
}

apm_must_have(){
    # theme
    #apm install native-ui
    #apm install seti-ui
    # packages
    apm install highlight-selected
    apm install file-icons
    apm install open-recent
    # optional
    apm install atom-beautify
    apm install platformio-ide-terminal
    apm install autocomplete-paths
    apm install tab-switcher
    # session share
    apm install teletype
    # split jump
    apm install jumpy
    # json
    apm install pretty-json
    apm install linter-jsonlint

    apm install linter
    apm install linter-pylint

    # auto update installed packages
    apm install auto-update-packages

    # wakatime integration
    apm install wakatime

    # docker highlight syntax
    apm install language-docker
    apm install linter-docker

    # gitlab manage pipes
    apm install gitlab-manager

    # kube and yaml
    apm install linter-kubectl
    apm install ide-yaml
    apm install linter-js-yaml

    # nginx conf support
    apm install language-nginx

    # ansible
    apm install language-ansible
    apm install autocomplete-ansible
    apm install linter-ansible-linting

    # md preview
    apm install markdown-preview
}

apm_optional(){
    apm install scroll-through-time
    apm install ansible-vault
    # force break time
    apm install take-a-break
    # just ctrl + alt + P
    apm install atom-password-generator
    # docker custom ui
    apm install docker
    # jenkins integration
    apm install jenkins
    # every script execution
    apm install script
}

apm_check
apm_must_have
#apm_optional
